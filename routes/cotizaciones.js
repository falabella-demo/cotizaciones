var express = require('express');
var app = express();

//Obtener cotizaciones
app.get('/', (req, res) => {

    var rut = req.query.rut;
    var birthdate = req.query.birthdate;
    var email = req.query.email;
    var tel = req.query.tel;

    if (rut == undefined || birthdate == undefined || email == undefined || tel == undefined) {
        res.status(500).json({
            ok: false,
            errors: "faltan parámetros obligatorios"
        });
    }

    res.status(200).json(getCotizaciones());

});

function getCotizaciones() {
    menu = [{
            plan: 'Plan 1',
            aseguradora: 'MetLife',
            monto_pesos: 11755,
            monto_uF: 0.41,
            capital_asegurado: 500,
            cuotas_gratis: [2, 6]
        },
        {
            plan: 'Plan 2',
            aseguradora: 'MetLife',
            monto_pesos: 23223,
            monto_uF: 0.81,
            capital_asegurado: 1000,
            cuotas_gratis: [2, 6]
        },
        {
            plan: 'Plan 3',
            aseguradora: 'MetLife',
            monto_pesos: 34978,
            monto_uF: 1.22,
            capital_asegurado: 1500,
            cuotas_gratis: [2, 6]
        }
    ];

    return menu;
}

module.exports = app;