# Cotizaciones Backend

## 1. Run Project Alternatives

## 1.1 Local

## Requirements
  - [**node**](https://nodejs.org/es/download/)
  - [**npm**](https://www.npmjs.com/get-npm)
 
## Steps
  
  1- npm install

  2- npm start
  
  3- show (http://localhost:3000)

## 1.1 Docker:

## Requirements

  - [**docker**](https://docs.docker.com/install/)

## Steps
  
  1- docker build -t cotizaciones:1.0 .

  2- docker run -d cotizaciones:1.0 -p 3000:3000

## 1.1 Kubernetes

## Requirements

  - [**docker**](https://docs.docker.com/install/)
  - [**kubernetes**](https://kubernetes.io/es/docs)

## Steps
  
  1- kubectl apply -f Deployment.yaml


