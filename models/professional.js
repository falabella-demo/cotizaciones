var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var professionalSchema = new Schema({
    name: { type: String, required: [true, 'El nombre es necesario'] },
    surname: { type: String, required: [true, 'El apellido es necesario'] },
    img: { type: String, required: false },
    user: { type: Schema.Types.ObjectId, ref: 'User', required: true },
    establishment: {
        type: Schema.Types.ObjectId,
        ref: 'Establishment',
        required: [true, 'El id	del establecimiento es un campo obligatorio ']
    }
}, { collection: 'professionals' });

module.exports = mongoose.model('Professional', professionalSchema);