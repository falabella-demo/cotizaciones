var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var turnSchema = new Schema({
    description: { type: String, required: true },
    note: { type: String },
    date: { type: Date, required: [true, 'La fecha de atención es requerida'] },
    pet: {
        type: Schema.Types.ObjectId,
        ref: 'Pet',
        required: [true, 'El id	de la mascota es un campo obligatorio ']
    },
    establishment: {
        type: Schema.Types.ObjectId,
        ref: 'Establishment',
        required: [true, 'El id	del establecimiento es un campo obligatorio ']
    },
    professional: {
        type: Schema.Types.ObjectId,
        ref: 'Professional'
    }
}, { collection: 'turns' });

module.exports = mongoose.model('Turn', turnSchema);